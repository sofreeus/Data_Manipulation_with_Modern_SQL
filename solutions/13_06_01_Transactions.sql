-- 13.6.1 Transactions

.echo on
begin;

insert into Mode_of_Transportation_Codes (mode, description) values
(30, 'paper airplane');

select * from Mode_of_Transportation_Codes;

rollback;

select * from Mode_of_Transportation_Codes;
