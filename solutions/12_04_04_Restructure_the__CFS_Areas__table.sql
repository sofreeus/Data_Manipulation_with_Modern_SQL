-- 12.4.4 Restructure the `CFS_Areas` table

.echo on

alter table CFS_Areas
    rename to CFS_Areas_Alt
;

create table CFS_Areas as
    select ORIG_MA as MA,
           ORIG_STATE as STATE,
           ORIG_CFS_AREA as CFS_AREA,
           MA as MA_Type,
           Description
    from CFS_Areas_Alt
;

delete from CFS_Areas where STATE = 'DEST_STATE';

drop table CFS_Areas_Alt;
