-- 14.2.3 What are the total values for all shipments by mode?

.header on
.mode column

select sum(SHIPMT_VALUE) Total_Value, Description from cfs
join Mode_of_Transportation_Codes mt using(mode)
group by mode
union 
select sum(SHIPMT_VALUE) Total_Value, 'Grand Total' from cfs
order by Total_Value
;
