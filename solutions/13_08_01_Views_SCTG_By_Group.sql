-- 13.8.1 Views SCTG By Group

create view "SCTG By Group" as 
select SCTG, '01-05' as Grp from SCTG where SCTG >= '01' and SCTG <= '05'
union
select SCTG, '06-09' as Grp from SCTG where SCTG >= '06' and SCTG <= '09'
union
select SCTG, '10-14' as Grp from SCTG where SCTG >= '10' and SCTG <= '14'
union
select SCTG, '15-19' as Grp from SCTG where SCTG >= '15' and SCTG <= '19'
union
select SCTG, '20-24' as Grp from SCTG where SCTG >= '20' and SCTG <= '24'
union
select SCTG, '25-30' as Grp from SCTG where SCTG >= '25' and SCTG <= '30'
union
select SCTG, '31-34' as Grp from SCTG where SCTG >= '31' and SCTG <= '34'
union
select SCTG, '35-38' as Grp from SCTG where SCTG >= '35' and SCTG <= '38'
union
select SCTG, '39-99' as Grp from SCTG where SCTG >= '39' and SCTG <= '99'
;

.mode column
.header on
select * from "SCTG By Group" order by Grp;
