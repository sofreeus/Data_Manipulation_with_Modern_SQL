-- 6.2.6 What state has the most counties?

select state, count(1) count 
from national_county group by statefp
order by count desc limit 1
