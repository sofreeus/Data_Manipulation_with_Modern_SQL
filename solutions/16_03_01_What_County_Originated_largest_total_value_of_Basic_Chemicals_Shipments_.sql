-- 16.3.1 What County Originated largest total value of Basic Chemicals Shipments?

.echo on

.mode column
.header on

with sctg_grp as ( select * from "SCTG By Group" 
        join SCTG using(sctg)
    ),
    bc_sctg as (
        select sctg from sctg_grp
        where grp in (
            select grp from sctg_grp where description = 'Basic Chemicals'
        )
    ),
    tsv as (
    select sum(SHIPMT_VALUE) Total_Shipment_Value,
        ORIG_STATE, ORIG_MA, ca.*
        from cfs
        join CFS_Areas ca on orig_state = ca.state and ORIG_MA = ca.ma 
        where cfs.sctg in (select sctg from bc_sctg )
        group by ORIG_STATE, ORIG_MA
        order by Total_Shipment_Value
    )
    select max(Total_Shipment_Value), Description from tsv;
