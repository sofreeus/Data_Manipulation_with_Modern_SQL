#! /bin/bash

# housekeeping
test -d db || mkdir db
test -f db/sqlite.db && rm db/sqlite.db

# load the data collected directly
sqlite3 db/sqlite.db << EOT
.echo on
.mode csv

drop table if exists cfs;
.import data/cfs_2012_pumf_csv.txt cfs

drop table if exists national_county;
.import data/national_county.txt national_county

.quit
EOT

# load the data extracted from the users guide
./load_meta

