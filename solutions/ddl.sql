-- Rename some tables

alter table NAICS_North_American_Industry_Classification_System_Codes
    rename to NAICS
;

alter table SCTG_Standard_Classification_of_Transported_Goods_Codes
    rename to SCTG
;
